from django.shortcuts import render
from .forms import FraudDetectionForm
import os
import json
import requests

# Create your views here.
def index(request):
    form = FraudDetectionForm()
    
    return render(request,'index.html',{'form': form})

def result(request):
    header = {
            'Content-Type': 'application/json',
        }
    
    user = request.POST.get('input1')
    amount = request.POST.get('input2')
    usechip = request.POST.get('input3')
    card = request.POST.get('input4')
    merchantName = request.POST.get('input5')
    errors = request.POST.get('input6')
    merchntState = request.POST.get('input7')
    merchantCity = request.POST.get('input8')
    zip = request.POST.get('input9')
    mcc = request.POST.get('input10')
    form = FraudDetectionForm()
    
    header = {
            'Content-Type': 'application/json',
            'Control': 'no-cache',
        }
    
    json_data = {
            'username' : os.getenv("USERNAME"),
            'password' : os.getenv("PASSWORD"),   
                      }

    response = requests.post('https://192.86.32.113:9888/auth/generateToken', json=json_data, headers=header,verify=False)

    token = json.loads(response.text)['token']

    header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
            'ML-Instance-Id' : 'ibmuser'
    }

    repository_url = 'http://169.38.75.202:8098/repository/CanaryDeployProdFraud/'
    json_file_path = 'scoringURL.json'

# Construct the URL for the JSON file in the Nexus repository
    url = f'{repository_url}/{json_file_path}'

    try:
    # Retrieve the JSON file from the Nexus repository
        response = requests.get(url)
        response.raise_for_status()  # Raise an exception if the request was unsuccessful

    # Parse the JSON data
        data = response.json()

    # Extract the desired value from the JSON data
        scoring_url = data['scoring_url']  # Replace 'key' with the actual key or path to the value

    # Use the extracted value
        print(f'The desired value is: {scoring_url}')
    except requests.exceptions.RequestException as e:
        print(f'Error: {e}')
    except KeyError:
        print('Error: The specified key does not exist in the JSON data.')
    except ValueError:
        print('Error: Failed to parse the JSON data.')

    payload_scoring = [{"Amount":amount,"Card":card,"Errors?":errors,"Merchant City":merchantCity,"Merchant Name":merchantName,"Merchant State":merchntState,"Use Chip":usechip,"User":user,"Zip":zip,"MCC":mcc}]

    response_scoring = requests.post(scoring_url, json=payload_scoring, headers=header,verify=False)

    json_out = (json.loads(response_scoring.text))

    prediction = json_out[0]['prediction']
    prob1 = json_out[0]['probability'][0]
    prob2 = json_out[0]['probability'][1]
    scoring_ID = scoring_url.split('/')[-1]

    return render(request,'result.html',{'form': form, 'user_profile':user,'mcc':mcc, 'amount': amount,'useChip':str(usechip), 'card': card,
                                            'merchantName':merchantName,'errors':errors,'merchantState':merchntState,
                                            'merchantCity':merchantCity, 'zip':zip, 'endpoint':scoring_ID, 
                                            'prediction':prediction,'prob1':prob1,'prob2':prob2})


